package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling {
	private static final int MAX_ROLLS = 20;
	//CAMPI
	private int[] rolls = new int[20];
	private int currentRoll = 0;

	//COSTRUTTORI

	//METODI
	@Override
	public void roll(int pins) {
		this.rolls[this.currentRoll++] = pins;
		if(isStrike(currentRoll-1))
			roll(0);
	}
	
	@Override
	public int score() {
		int partialScore = 0;
		for(int currentRoll=0; currentRoll<MAX_ROLLS; currentRoll++){
			if( !isStrike(currentRoll) && isSpare(currentRoll))
				partialScore += this.rolls[currentRoll+2];
			partialScore += this.rolls[currentRoll];
		}
		int strikeScore = calcolaBonusScoreStrike();
		return partialScore + strikeScore;
	}

	private int calcolaBonusScoreStrike() {
		int strikeScore = 0;
		for(int cercaStrike=0; cercaStrike<MAX_ROLLS-2; cercaStrike++){
			if( isStrike(cercaStrike) )
				strikeScore = strikeScore + (rolls[cercaStrike+2] + rolls[cercaStrike+3]);
		}
		return strikeScore;
	}
	
	private boolean isStrike(int roll){
		return rolls[roll] == 10 && rolls[roll]%2 == 0;
	}
	
	private boolean isSpare(int currentRoll){
		return  currentRoll%2 == 0 && 
				currentRoll < MAX_ROLLS-1 &&
				rolls[currentRoll] + rolls [currentRoll+1] == 10;
	}
}